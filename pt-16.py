d = {   'India' :  'Rupee',
        'UK' : 'Pound',
        'France':'Euro',
        'Japan' : 'Yen',
        'Austria' : 'Euro',
        'Bangladesh': 'Taka',
        'Italy' : 'Euro'
     }
L = [country for country,currency in d.items() if currency == 'Yen']
