names  = ['Ted', 'Sam', 'Jim', 'Rob', 'Anu'] 
maths = [98,67,54,88,95]
physics = [88,64,78,99,78]
chemistry = [78,67,45,79,87]

d = {name : [m1,m2,m3] for name, m1,m2,m3 in zip(names,maths,physics,chemistry) }


{ 'Ted': {'Maths': 98, 'Physics': 88, 'Chemistry': 78}, 
  'Sam': {'Maths': 67, 'Physics': 64, 'Chemistry': 67}, 
  'Jim': {'Maths': 54, 'Physics': 78, 'Chemistry': 45}, 
  'Rob': {'Maths': 88, 'Physics': 99, 'Chemistry': 79}, 
  'Anu': {'Maths': 95, 'Physics': 78, 'Chemistry': 87}  }

d = {name : {'Maths':m1,'Physics':m2,'Chemistry':m3} for name, m1,m2,m3 in zip(names,maths,physics,chemistry) }
