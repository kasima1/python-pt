class Bank_account:
    def set_details(self,name,balance=0) :
        self.name = name
        self.balance = balance
        
    def display(self):
        print (self.name, self.balance)
    
    def withdraw(self, amount):
        self.balance -= amount
        
    def deposit(self, amount) :
        self.balance += amount
                
a1 = Bank_account()
a2 = Bank_account()
a1.set_details('MIke',200)
a2.set_details('Tom')

a1.display()
a2.display()

a1.withdraw(200)
a2.deposit(500)

a1.display()
a2.display()
